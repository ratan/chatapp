import os
import webapp2
import jinja2
import hmac
import re
import hashlib
import urllib2
import json
import logging
import time
import random
from xml.dom import minidom
from google.appengine.api import memcache
from google.appengine.ext import db
from datetime import datetime

template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir), autoescape = True)



GENDER = (
    (0,'MALE'),
    (1,'FEMALE')
)



SECRET = 'ghostkeyforchatstranger'



def hash_str(s):
    return hmac.new(SECRET, s).hexdigest()

def make_secure_val(s):
    return "%s|%s" % (s, hash_str(s))

def check_secure_val(h):
    val = h.split('|')[0]
    if h == make_secure_val(val):
        return val

def render_str(template, **params):
    t = jinja_env.get_template(template)
    return t.render(params)



GMAPS_URL = "http://maps.googleapis.com/maps/api/staticmap?size=850x250&sensor=false&"


def gmaps_img(points):
    markers = '&'.join('markers=%s,%s' % (p.lat, p.lon)
                       for p in points)
    return GMAPS_URL + markers

IP_URL = "http://api.hostip.info/?ip="
def get_coords(ip):
    url = IP_URL + ip
    content = None
    try:
        content = urllib2.urlopen(url).read()
    except:
        return
    
    if content:
        d = minidom.parseString(content)
        coords = d.getElementsByTagName("gml:coordinates")
        if coords and coords[0].childNodes[0].nodeValue:
            lon, lat = coords[0].childNodes[0].nodeValue.split(',')
            return db.GeoPt(lat, lon)
        
class BaseHandler(webapp2.RequestHandler):
    def render(self, template, **kw):
        self.response.out.write(render_str(template, **kw))
        
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)
        

class UsersDB(db.Model):
    username = db.StringProperty(required = True)
    first_name = db.StringProperty()
    last_name = db.StringProperty()
    gender = db.IntegerProperty(choices = GENDER)
    age = db.IntegerProperty()
    city = db.StringProperty()
    country = db.StringProperty()
    password = db.StringProperty(required = True)
    email = db.StringProperty(required = True)
    img = db.StringProperty()


class VisitsDB(db.Model):
    user = db.ReferenceProperty(UsersDB,required=False)
    visit = db.IntegerProperty(required = True,default=0)


class ChatDB(db.Model):
    sender = db.ReferenceProperty(UsersDB,required=True,collection_name='sender_chat')
    recipient = db.ReferenceProperty(UsersDB,required=True,collection_name='recipient_chat')
    timestamp = db.DateTimeProperty(auto_now_add=True)
    status = db.BooleanProperty(default=True)




class MessageDB(db.Model):
    chat = db.ReferenceProperty(ChatDB,required=True)
    sender = db.ReferenceProperty(UsersDB,required=True,collection_name='sender')
    recipient = db.ReferenceProperty(UsersDB,required=True,collection_name='recipient')
    message = db.StringProperty(required = True)
    timestamp = db.DateTimeProperty(auto_now_add=True)
    delivered = db.BooleanProperty(default=False)





USER_RE = re.compile(r"^[a-zA-Z0-9_-]{3,20}$")
def valid_username(username):
    return username and USER_RE.match(username)

PASS_RE = re.compile(r"^.{3,20}$")
def valid_password(password):
    return password and PASS_RE.match(password)

EMAIL_RE  = re.compile(r'^[\S]+@[\S]+\.[\S]+$')
def valid_email(email):
    return not email or EMAIL_RE.match(email)




class Home(BaseHandler):
    def get(self):
        username =self.request.cookies.get('username')
        email = self.request.cookies.get('email')
        token = self.request.cookies.get('token')
        user_key = self.request.cookies.get('user_key')
        vnew = VisitsDB.get_or_insert(key_name="site",name='visit')
        vnew.visit = vnew.visit + 1
        vnew.put()
        user = []
        if not username:
                self.redirect('/login')
        if str(token) == str(hash_str(str(username))):
            user = UsersDB.get_by_id(long(user_key))
            #print 



            chat1 = db.GqlQuery('SELECT * FROM ChatDB WHERE sender=:1',user)
            chat2 = db.GqlQuery('SELECT * FROM ChatDB WHERE recipient=:1',user)
            chats =()
            for c in chat1:
                chats = chats+(c,)
            for c in chat2:
                if c not in chats:
                    chats = chats+(c,)
            
            msg1 = db.GqlQuery('SELECT * FROM MessageDB WHERE sender=:1',user)
            sent_msgs=[]
            for sent in msg1:
                sent_msgs.append(sent)

            msg2 = db.GqlQuery('SELECT * FROM MessageDB WHERE recipient=:1',user)
            recieved_msgs=[]
            for recieve in msg2:
                    recieved_msgs.append(recieve)


            msgs = sent_msgs
            msgs.extend(recieved_msgs)
            print msgs
            users=UsersDB.all().fetch(100)
            for k in users:
                if k.key().id()==user.key().id():
                    users.remove(k)
            #print "###########@@@@@@@@@@@@@@@#######################"
            #print test
            #print "###########@@@@@@@@@@@@@@@#######################"
            msgs = sorted(msgs,key= lambda x : x.timestamp)

            self.render("index.html",visits=vnew.visit,user=user,users=users,chats=chats,msgs=msgs)
        else:
            self.redirect('/login')
            





class Signup(BaseHandler):
    def get(self):
        #import ipdb;ipdb.set_trace()
        self.render("signup.html")
    
    def post(self):
        have_error = False
        username = self.request.get('username')
        password = self.request.get('password')
        verify = self.request.get('password')
        city = self.request.get('city')
        country = self.request.get('country')
        email = self.request.get('email')

        params = dict(username = username,
                      email = email)

        if not valid_username(username):
            params['error_username'] = "That's not a valid username."
            have_error = True
        u = db.GqlQuery('SELECT * FROM UsersDB WHERE username=:1', str(username))
        for i in u:
            if i.username==username:
                params['error_username'] = 'User already exists!'
                have_error = True
        if not valid_password(password):
            params['error_password'] = "That wasn't a valid password."
            have_error = True
        elif password != verify:
            params['error_verify'] = "Your passwords didn't match."
            have_error = True
        if email: 
            if not valid_email(email):
                params['error_email'] = "That's not a valid email."
                have_error = True

        if have_error:
            self.render('/signup', **params)
        else:
            u = UsersDB(username = username, password = hashlib.sha256(password).hexdigest(), email = email,city = city,country = country )
            u.put()
            self.response.headers.add_header('Set-Cookie', 'username=%s;Path=/' % str(username))
            self.response.headers.add_header('Set-Cookie', 'email=%s;Path=/' % str(email))
            self.response.headers.add_header('Set-Cookie', 'token=%s;Path=/' % str(hash_str(str(username))))
            self.response.headers.add_header('Set-Cookie', 'user_key=%s;Path=/' % str(u.key().id()))
            self.redirect('/home')
            # self.render("index.html",user=user)

class Login(BaseHandler):
    def get(self):
        self.render('login.html')
        
    def post(self):
        username = self.request.get('username')
        password = self.request.get('password')
        # #print self.request.get('username')
        # #print self.request.get('password')
        error_username = "0"
        error_password = "0"
        email=""
        u = db.GqlQuery("SELECT * FROM UsersDB WHERE username=:1", username)
        for i in u:
            if i.username == username:
                if i.password == hashlib.sha256(password).hexdigest():
                    email = i.email
                    user_key = i.key().id()
                    self.response.headers.add_header('Set-Cookie', 'username=%s;Path=/' % str(username))
                    self.response.headers.add_header('Set-Cookie', 'email=%s;Path=/' % str(email))
                    self.response.headers.add_header('Set-Cookie', 'user_key=%s;Path=/' % str(user_key))
                    self.response.headers.add_header('Set-Cookie', 'token=%s;Path=/' % str(hash_str(str(username))))
                    self.redirect('/home')
                else:
                    error_password = "1"
            else:
                error_username = "1"
        self.render("login.html", epassword = error_password,eusername=error_username)
                


class Logout(BaseHandler):
    def get(self):
        self.response.headers.add_header('Set-Cookie', "user=;Path=/")
        self.response.headers.add_header('Set-Cookie', "email=;Path=/")
        self.response.headers.add_header('Set-Cookie', "token=;Path=/")
        self.response.headers.add_header('Set-Cookie', "user_key=;Path=/")
        self.redirect("/login") 


         
class Welcome(BaseHandler):
    def get(self):
        self.render('welcome.html')



class Sendmsg(BaseHandler):
    def get(self):
        self.redirect("/login")

    def post(self):
        # #print"########################"
        # #print self.request.get('recipients')
        # #print self.request.get('msg')
        # #print self.request.cookies.get('username')
        # #print"########################"
        recipients = self.request.get('recipients').split(',')
        message = self.request.get('msg')
        user_key = self.request.cookies.get('user_key')
        #print recipients
        chat =[]
        for recipient in recipients:
            flag = True
            to_user = UsersDB.get_by_id(long(recipient))
            from_user = UsersDB.get_by_id(long(user_key))
            key_name = long(''.join(str(ord(c)) for c in from_user.username))+long(''.join(str(ord(c)) for c in to_user.username))
            chat = ChatDB.get_or_insert(key_name=str(key_name),sender=from_user,recipient=to_user)
            msg = MessageDB(chat = chat , message = message,sender=from_user,recipient=to_user)
            msg.put()
        self.redirect("/home")

class Msgsend(BaseHandler):
    def post(self):
        frm = self.request.get('from')
        ##print "sender : "+str(frm)
        to = self.request.get('to')
        ##print "recipient : "+str(to)
        message = self.request.get('msg')
        key_name = self.request.get('key')
        from_user = UsersDB.get_by_id(long(frm))
        #print from_user.username
        to_user = UsersDB.get_by_id(long(to))
        #print to_user.username
        print "#############################"
        chat = ChatDB.get(keys=key_name)
        # u = db.GqlQuery("SELECT * FROM ChatDB WHERE sender=:1 AND recipient=:2",from_user,to_user)
        # print u
        # for c in u:
        #     chat = c
        msg = MessageDB(chat = chat , message = message,sender=from_user,recipient=to_user)
        msg.put()
        print "#############123#############"
        # print UsersDB.get_by_id(long(c.sender))
        # print UsersDB.get_by_id(long(c.recipient))
        print msg.message
        print msg.is_saved()
        print "#############123#############"
            





app = webapp2.WSGIApplication([('/signup/?', Signup),
                               ('/welcome/?', Welcome),
                               ('/login/?', Login),
                               ('/logout/?', Logout),
                               ('/home/?',Home),
                               ('/sendmsg/?',Sendmsg),
                               ('/msgsend/?',Msgsend),
                               ('/?',Welcome)
                               ],debug=True)
        